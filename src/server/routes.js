var router = require('express').Router();
var four0four = require('./utils/404')();
var data = require('./data');

router.get('/cards', getCards);
router.get('/staff', getStaff);
router.get('/stations', getStations);
router.get('/services', getServices);
router.get('/person/:id', getPerson);
router.get('/*', four0four.notFoundMiddleware);

module.exports = router;

//////////////

function getCards(req, res, next) {
  res.status(200).send(data.cards);
}

function getStaff(req, res, next) {
  res.status(200).send(data.staff);
}

function getStations(req, res, next) {
  res.status(200).send(data.stations);
}

function getServices(req, res, next) {
  res.status(200).send(data.services);
}

function getPerson(req, res, next) {
  var id = +req.params.id;
  var person = data.staff.filter(function(p) {
    return p.id === id;
  })[0];

  if (person) {
    res.status(200).send(person);
  } else {
    four0four.send404(req, res, 'person ' + id + ' not found');
  }
}
