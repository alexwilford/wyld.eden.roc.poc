module.exports = {
  staff: getStaff(),
  stations: getStations(),
  services: getServices(),
  cards: getCards()
};

function getStaff() {
  return [{
    id: 1,
    no: '011926',
    fn: 'Gene',
    name: 'Gene Myers',
    role: 'Director',
    office: 'Hove',
    group: 'KGX-0055',
    station: 'KGX',
    headcode: 'KGX-0055',
    location: 'Kings Cross',
    lastseen: '14 mins ago',
    min: 14,
    status: 'G',
    online: 'Online',
    symbol: 'fa fa-check-circle',
    device: 'D807F373-094A-4D9B-975B-291345C00182',
    dname: 'WYLD-02-5s',
    image: 'images/team/gm.png',
    selected: true
  }, {
    id: 2,
    no: '028495',
    fn: 'Craig',
    name: 'Craig Sheridon',
    role: 'Director',
    office: 'Edinburgh',
    group: 'EDB-0077',
    station: 'EDB',
    headcode: 'EDB-0077',
    location: 'Edinburgh',
    lastseen: '4 hours ago',
    min: 400,
    status: 'R',
    online: 'Offline',
    symbol: 'fa fa-clock-o',
    device: '24269450-8231-0498-2878-159338927345',
    dname: 'WYLD-01-02',
    image: 'images/team/cs.png',
    selected: true
  }, {
    id: 3,
    no: '029286',
    fn: 'Erika',
    name: 'Erika Reid',
    role: 'Manager',
    office: 'Edinburgh',
    group: 'EDB-0077',
    station: 'EDB',
    headcode: 'EDB-0077',
    location: 'Edinburgh',
    lastseen: '7 mins ago',
    min: 7,
    status: 'G',
    online: 'Online',
    symbol: 'fa fa-check-circle',
    device: '93269450-8231-0498-2878-159338927317',
    dname: 'WYLD-01-03',
    image: 'images/team/er.png',
    selected: true
  }, {
    id: 4,
    no: '110732',
    fn: 'Nicola',
    name: 'Nicola Fraser-Liddy',
    role: 'Manager',
    office: 'Hove',
    group: 'YRK-0055',
    station: 'YRK',
    headcode: 'YRK-0055',
    location: 'York',
    lastseen: '4 mins ago',
    min: 4,
    status: 'G',
    online: 'Online',
    symbol: 'fa fa-check-circle',
    device: 'F7B971D6-2B0C-46DA-B4DD-63718A8FD53F',
    dname: 'WYLD-03-5s',
    image: 'images/team/nf.png',
    selected: true
  }, {
    id: 5,
    no: '028778',
    fn: 'Raheel',
    name: 'Raheel Modassar',
    role: 'Manager',
    office: 'India',
    group: 'LDS-0055',
    station: 'LDS',
    headcode: 'LDS-0055',
    location: 'Leeds',
    lastseen: '2 mins ago',
    min: 2,
    status: 'G',
    online: 'Online',
    symbol: 'fa fa-check-circle',
    device: '73269450-8231-0498-2878-159338927314',
    dname: 'WYLD-01-05',
    image: 'images/team/rm.png',
    selected: true
  }, {
    id: 6,
    no: '257991',
    fn: 'Ben',
    name: 'Ben Wood',
    role: 'Station Master',
    office: 'Hove',
    group: 'BWK-0055',
    station: 'BWK',
    headcode: 'BWK-0055',
    location: 'Berwick',
    lastseen: '42 mins ago',
    min: 42,
    status: 'A',
    online: 'Away',
    symbol: 'fa fa-clock-o',
    device: '73401F8B-9683-4A31-ADFD-CE85992F68F9',
    dname: 'WYLD-04-05',
    image: 'images/team/bw.png',
    selected: true
  }, {
    id: 7,
    no: '268377',
    fn: 'Alex',
    name: 'Alex Wilford',
    role: 'Station Master',
    office: 'Hove',
    group: 'LCN-0055',
    station: 'LCN',
    headcode: 'LCN-0055',
    location: 'Lincoln',
    lastseen: '12 hours ago',
    min: 1000,
    status: 'R',
    online: 'Offline',
    symbol: 'fa fa-clock-o',
    device: 'AE244AD8-DE78-416C-BE53-DD82E5570895',
    dname: 'WYLD-03-5s',
    image: 'images/team/aw.png',
    selected: true
  }, {
    id: 8,
    no: '311926',
    fn: 'Vern',
    name: 'Vern Smith',
    role: 'Station Master',
    office: 'Harrogate',
    group: 'HGT-0099',
    station: 'HGT',
    headcode: 'HGT-0099',
    location: 'Harrogate',
    lastseen: '2 hours ago',
    min: 120,
    status: 'A',
    online: 'Away',
    symbol: 'fa fa-clock-o',
    device: '63269450-8231-0498-2878-159338927333',
    dname: 'WYLD-01-08',
    image: 'images/team/gm.png',
    selected: true
  }, {
    id: 9,
    no: '428495',
    fn: 'Rob',
    name: 'Rob Carmichael',
    role: 'Station Assistant',
    office: 'Kings Cross',
    group: 'KGX-0099',
    station: 'KGX',
    headcode: 'KGX-0099',
    location: 'Kings Cross',
    lastseen: '5 hours ago',
    min: 540,
    status: 'R',
    online: 'Offline',
    symbol: 'fa fa-clock-o',
    device: '71269450-8231-0498-2878-159338927325',
    dname: 'WYLD-01-09',
    image: 'images/team/cs.png',
    selected: true
  }, {
    id: 10,
    no: '529286',
    fn: 'Yvonne',
    name: 'Yvonne Heyman',
    role: 'Incident Manager',
    office: 'Leeds',
    group: 'LDS-0099',
    station: 'LDS',
    headcode: 'LDS-0099',
    location: 'Leeds',
    lastseen: '6 hours ago',
    min: 600,
    status: 'R',
    online: 'Offline',
    symbol: 'fa fa-clock-o',
    device: '55269450-8231-0498-2878-159338927332',
    dname: 'WYLD-01-10',
    image: 'images/team/er.png',
    selected: true
  }, {
    id: 11,
    no: '610732',
    fn: 'Tanya',
    name: 'Tanya Swift',
    role: 'Station Master',
    office: 'Leeds',
    group: 'LDS-0099',
    station: 'LDS',
    headcode: 'LDS-0099',
    location: 'Leeds',
    lastseen: '1 hour ago',
    min: 60,
    status: 'A',
    online: 'Away',
    symbol: 'fa fa-clock-o',
    device: '73269450-8231-0498-2878-159338927301',
    dname: 'WYLD-01-11',
    image: 'images/team/nf.png',
    selected: true
  }, {
    id: 12,
    no: '728778',
    fn: 'Zee',
    name: 'Zee James',
    role: 'Incident Manager',
    office: 'Peterborough',
    group: 'PBO-0099',
    station: 'PBO',
    headcode: 'PBO-0099',
    location: 'Peterborough',
    lastseen: '56 mins ago',
    min: 56,
    status: 'A',
    online: 'Away',
    symbol: 'fa fa-clock-o',
    device: '02269450-8231-0498-2878-159338927319',
    dname: 'WYLD-01-12',
    image: 'images/team/rm.png',
    selected: true
  }, {
    id: 13,
    no: '857991',
    fn: 'Simon',
    name: 'Simon Arndale',
    role: 'Station Assistant',
    office: 'Peterborough',
    group: 'PBO-0099',
    station: 'PBO',
    headcode: 'PBO-0099',
    location: 'Peterborough',
    lastseen: '1 hour ago',
    min: 60,
    status: 'A',
    online: 'Away',
    symbol: 'fa fa-clock-o',
    device: '88269450-8231-0498-2878-159338927303',
    dname: 'WYLD-01-13',
    image: 'images/team/bw.png',
    selected: true
  }, {
    id: 14,
    no: '968377',
    fn: 'Sam',
    name: 'Sam Weaver',
    role: 'Incident Assistant',
    office: 'York',
    group: 'YRK-0099',
    station: 'YRK',
    headcode: 'YRK-0099',
    location: 'York',
    lastseen: '5 hours ago',
    min: 500,
    status: 'R',
    online: 'Offline',
    symbol: 'fa fa-clock-o',
    device: '45269450-8231-0498-2878-159338927399',
    dname: 'WYLD-01-14',
    image: 'images/team/aw.png',
    selected: true
  }];
}

function getCards() {
  return [{
    id: 1,
    subject: 'Trains running',
    value: '105',
    sub: '2 trains out of service',
    symbol: 'fa fa-train',
    link: ''
  }, {
    id: 2,
    subject: 'Service alterations',
    value: '36',
    sub: '4 cancellations',
    symbol: 'fa fa-clock-o',
    link: ''
  }, {
    id: 3,
    subject: 'Likely to fail PPM',
    value: '14',
    sub: '6 PPM failures confirmed',
    symbol: 'fa fa-train',
    link: ''
  }, {
    id: 4,
    subject: 'Current PPM',
    value: '94.2%',
    sub: 'Max achievable PPM 96.2%',
    symbol: 'fa fa-bar-chart',
    link: ''
  }];
}

function getServices() {
  return [{
    id: 1,
    train: '1Y86',
    service: '1402 York to Kings Cross',
    issue: '12 mins late',
    ppm: 'On target',
    status: 'G'
  }, {
    id: 2,
    train: '1Y11',
    service: '0630 Newcastle to Kings Cross',
    issue: 'Cancelled',
    ppm: 'Failed',
    status: 'R'
  }, {
    id: 3,
    train: '1Y14',
    service: '0600 Berwick to Kings Cross',
    issue: 'Started at Peterborough',
    ppm: 'On target',
    status: 'G'
  }, {
    id: 4,
    train: '1E08',
    service: '0900 Edinburgh to Kings Cross',
    issue: 'Terminated at York',
    ppm: 'Failed',
    status: 'R'
  }, {
    id: 5,
    train: '1D19',
    service: '1505 Kings Cross to Leeds',
    issue: 'Starting at Doncaster',
    ppm: 'On target',
    status: 'G'
  }];
}

// array of stations
function getStations() {
  return [{
    id: 2245,
    level: 1,
    stationid: '3941320805809',
    code: 'ABD',
    name: 'Aberdeen',
    lat: 57.143204,
    lng: -2.098584
  }, {
    id: 2288,
    level: 2,
    stationid: '3402410729816',
    code: 'DEE',
    name: 'Dundee',
    lat: 56.456746,
    lng: -2.971234
  }, {
    id: 2227,
    level: 1,
    stationid: '2667770845463',
    code: 'INV',
    name: 'Inverness',
    lat: 57.479854,
    lng: -4.223568
  }, {
    id: 2281,
    level: 2,
    stationid: '3112920722913',
    code: 'PTH',
    name: 'Perth',
    lat: 56.390851,
    lng: -3.439142
  }, {
    id: 2402,
    level: 1,
    stationid: '2587720665117',
    code: 'GLC',
    name: 'Glasgow',
    lat: 55.858536,
    lng: -4.257961
  }, {
    id: 2522,
    level: 3,
    stationid: '3239820673144',
    code: 'HYM',
    name: 'Haymarket',
    lat: 55.945346,
    lng: -3.218738
  }, {
    id: 2523,
    level: 1,
    stationid: '3257830673840',
    code: 'EDB',
    name: 'Edinburgh',
    lat: 55.95188,
    lng: -3.1901
  }, {
    id: 2537,
    level: 2,
    stationid: '3680790678464',
    code: 'DUN',
    name: 'Dunbar',
    lat: 55.998103,
    lng: -2.51336
  }, {
    id: 1992,
    level: 1,
    stationid: '3993950653420',
    code: 'BWK',
    name: 'Berwick',
    lat: 55.77415,
    lng: -2.011204
  }, {
    id: 3,
    level: 3,
    stationid: '4230830611074',
    code: 'ALM',
    name: 'Alnmouth',
    lat: 55.393113,
    lng: -1.637126
  }, {
    id: 53,
    level: 3,
    stationid: '4203000585389',
    code: 'MPT',
    name: 'Morpeth',
    lat: 55.162436,
    lng: -1.682906
  }, {
    id: 57,
    level: 2,
    stationid: '4397010556958',
    code: 'SUN',
    name: 'Sunderland',
    lat: 54.905789,
    lng: -1.382344
  }, {
    id: 46,
    level: 1,
    stationid: '4245510563772',
    code: 'NCL',
    name: 'Newcastle',
    lat: 54.967993,
    lng: -1.618038
  }, {
    id: 44,
    level: 1,
    stationid: '4269800542781',
    code: 'DUR',
    name: 'Durham',
    lat: 54.779237,
    lng: -1.582053
  }, {
    id: 39,
    level: 2,
    stationid: '4294240513900',
    code: 'DAR',
    name: 'Darlington',
    lat: 54.519558,
    lng: -1.546953
  }, {
    id: 298,
    level: 2,
    stationid: '4363910493227',
    code: 'NTR',
    name: 'Northallerton',
    lat: 54.333324,
    lng: -1.441858
  }, {
    id: 327,
    level: 1,
    stationid: '4595340451611',
    code: 'YRK',
    name: 'York',
    lat: 53.957171,
    lng: -1.094189
  }, {
    id: 233,
    level: 2,
    stationid: '3983740451374',
    code: 'SKI',
    name: 'Skipton',
    lat: 53.958453,
    lng: -2.026272
  }, {
    id: 297,
    level: 1,
    stationid: '4304390455301',
    code: 'HGT',
    name: 'Harrogate',
    lat: 53.99286,
    lng: -1.537208
  }, {
    id: 273,
    level: 1,
    stationid: '4299810433239',
    code: 'LDS',
    name: 'Leeds',
    lat: 53.794267,
    lng: -1.548657
  }, {
    id: 1122,
    level: 2,
    stationid: '5090660428750',
    code: 'HUL',
    name: 'Hull',
    lat: 53.743721,
    lng: -0.347699
  }, {
    id: 319,
    level: 2,
    stationid: '4571100403170',
    code: 'DON',
    name: 'Doncaster',
    lat: 53.522095,
    lng: -1.14006
  }, {
    id: 643,
    level: 2,
    stationid: '4701670380309',
    code: 'RET',
    name: 'Retford',
    lat: 53.31506,
    lng: -0.948231
  }, {
    id: 644,
    level: 2,
    stationid: '4703860380172',
    code: 'RET',
    name: 'Retford',
    lat: 53.3138,
    lng: -0.944975
  }, {
    id: 656,
    level: 1,
    stationid: '4975710370891',
    code: 'LCN',
    name: 'Lincoln',
    lat: 53.226095,
    lng: -0.539899
  }, {
    id: 647,
    level: 2,
    stationid: '4805010354518',
    code: 'NNG',
    name: 'Newark North Gate',
    lat: 53.081807,
    lng: -0.799642
  }, {
    id: 653,
    level: 2,
    stationid: '4913770335156',
    code: 'GRA',
    name: 'Grantham',
    lat: 52.906056,
    lng: -0.642786
  }, {
    id: 1211,
    level: 1,
    stationid: '5186530298862',
    code: 'PBO',
    name: 'Peterborough',
    lat: 52.574611,
    lng: -0.25052
  }, {
    id: 1219,
    level: 2,
    stationid: '5234560224073',
    code: 'SVG',
    name: 'Stevenage',
    lat: 51.901486,
    lng: -0.206979
  }, {
    id: 1674,
    level: 1,
    stationid: '5302720183235',
    code: 'KGX',
    name: 'Kings Cross',
    lat: 51.531817,
    lng: -0.123399
  }];
}
