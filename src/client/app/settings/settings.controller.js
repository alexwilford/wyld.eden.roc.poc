(function() {
  'use strict';

  angular
    .module('app.settings')
    .controller('SettingsController', SettingsController);

  SettingsController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function SettingsController(logger, dataservice) {
    var vm = this;
    vm.title = 'Settings';

    activate();

    function activate() {
        var userTheme = dataservice.getUserConfig('theme');
        vm.defaultTheme = (userTheme !== 'dark');
        dataservice.setTheme(userTheme);
        logger.info('Activated Settings View');
    }

    vm.setTheme = function(theme) {
        dataservice.setTheme(theme);
    };

  }
})();
