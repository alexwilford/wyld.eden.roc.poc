/* jshint -W117, -W030 */
describe('stations routes', function() {
  describe('state', function() {
    var view = 'app/stations/stations.html';

    beforeEach(function() {
      module('app.stations', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state stations to url /stations ', function() {
      expect($state.href('stations', {})).to.equal('/stations');
    });

    it('should map /stations route to stations View template', function() {
      expect($state.get('stations').templateUrl).to.equal(view);
    });

    it('of stations should work with $state.go', function() {
      $state.go('stations');
      $rootScope.$apply();
      expect($state.is('stations'));
    });
  });
});
