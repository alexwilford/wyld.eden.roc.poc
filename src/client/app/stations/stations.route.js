(function() {
  'use strict';

  angular
    .module('app.stations')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'stations',
        config: {
          url: '/stations',
          templateUrl: 'app/stations/stations.html',
          controller: 'StationsController',
          controllerAs: 'vm',
          title: 'Stations',
          settings: {
            nav: 4,
            content: '<i class="fa fa-map-signs" title="Stations"></i>'
          }
        }
      }
    ];
  }
})();
