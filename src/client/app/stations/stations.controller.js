(function() {
  'use strict';

  angular
    .module('app.stations')
    .controller('StationsController', StationsController);

  StationsController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function StationsController(logger, dataservice) {
    var vm = this;
    vm.title = 'Stations';
    vm.subtitle = 'Station Information';

    activate();

    function activate() {
      dataservice.setTheme();
      logger.info('Activated Stations View');
    }
  }
})();
