(function() {
  'use strict';

  angular.module('app.stations', [
    'app.core',
    'app.widgets'
  ]);

})();
