(function() {
  'use strict';

  angular.module('app', [
    'nemLogging',
    'ui-leaflet',
    'app.core',
    'app.widgets',
    'app.dashboard',
    'app.routes',
    'app.trains',
    'app.stations',
    'app.disrupts',
    'app.comms',
    'app.settings',
    'app.admin',
    'app.layout'
  ]);

})();
