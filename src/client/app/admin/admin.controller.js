(function() {
  'use strict';

  angular
    .module('app.admin')
    .controller('AdminController', AdminController);

  AdminController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function AdminController(logger, dataservice) {
    var vm = this;
    vm.title = 'Admin';

    activate();

    function activate() {
      dataservice.setTheme();
      logger.info('Activated Admin View');
    }
  }
})();
