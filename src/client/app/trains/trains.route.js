(function() {
  'use strict';

  angular
    .module('app.trains')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'trains',
        config: {
          url: '/trains',
          templateUrl: 'app/trains/trains.html',
          controller: 'TrainsController',
          controllerAs: 'vm',
          title: 'Trains',
          settings: {
            nav: 3,
            content: '<i class="fa fa-train" title="Trains"></i>'
          }
        }
      }
    ];
  }
})();
