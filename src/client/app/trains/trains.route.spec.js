/* jshint -W117, -W030 */
describe('trains routes', function() {
  describe('state', function() {
    var view = 'app/trains/trains.html';

    beforeEach(function() {
      module('app.trains', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state trains to url /trains ', function() {
      expect($state.href('trains', {})).to.equal('/trains');
    });

    it('should map /trains route to trains View template', function() {
      expect($state.get('trains').templateUrl).to.equal(view);
    });

    it('of trains should work with $state.go', function() {
      $state.go('trains');
      $rootScope.$apply();
      expect($state.is('trains'));
    });
  });
});
