(function() {
  'use strict';

  angular
    .module('app.trains')
    .controller('TrainsController', TrainsController);

  TrainsController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function TrainsController(logger, dataservice) {
    var vm = this;
    vm.title = 'Trains';
    vm.subtitle = 'Train Information';

    activate();

    function activate() {
      dataservice.setTheme();
      logger.info('Activated Trains View');
    }
  }
})();
