(function() {
  'use strict';

  angular.module('app.trains', [
    'app.core',
    'app.widgets'
  ]);

})();
