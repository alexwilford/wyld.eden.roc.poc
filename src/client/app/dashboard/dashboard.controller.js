(function() {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardController', DashboardController);

  DashboardController.$inject = ['$q', 'logger', 'dataservice'];
  /* @ngInject */
  function DashboardController($q, logger, dataservice) {
    var vm = this;
    vm.news = {
      title: 'EDEN',
      description: 'EDEN Network Ops Centre'
    };

    vm.title = 'Dashboard';
    vm.subtitle = 'VTEC Dashboard';
    vm.messageCount = 0;
    vm.staff = [];
    vm.cards = [];
    vm.services = [];
    vm.date = new Date();

    activate();

    function activate() {
      dataservice.setTheme();
      var promises = [
          getStaff(),
          getCards(),
          getServices(),
      ];
      return $q.all(promises).then(function() {
        logger.info('Activated Dashboard View');
      });
    }

    function getMessageCount() {
      return dataservice.getMessageCount().then(function(data) {
        vm.messageCount = data;
        return vm.messageCount;
      });
    }

    function getStaff() {
      return dataservice.getStaff().then(function(data) {
        vm.staff = data;
        return vm.staff;
      });
    }

    function getCards() {
      return dataservice.getCards().then(function(data) {
        vm.cards = data;
        return vm.cards;
      });
    }

    function getServices() {
      return dataservice.getServices().then(function(data) {
        vm.services = data;
        return vm.services;
      });
    }

  }
})();
