(function() {
  'use strict';

  angular
    .module('app.routes')
    .controller('RoutesController', RoutesController);

  RoutesController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function RoutesController(logger, dataservice) {
    var vm = this;
    vm.title = 'Routes';
    vm.subtitle = 'Route Information';

    activate();

    function activate() {
      dataservice.setTheme();
      logger.info('Activated Routes View');
    }
  }
})();
