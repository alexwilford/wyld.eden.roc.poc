/* jshint -W117, -W030 */
describe('routes routes', function() {
  describe('state', function() {
    var view = 'app/routes/routes.html';

    beforeEach(function() {
      module('app.routes', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state routes to url /routes ', function() {
      expect($state.href('routes', {})).to.equal('/routes');
    });

    it('should map /routes route to routes View template', function() {
      expect($state.get('routes').templateUrl).to.equal(view);
    });

    it('of routes should work with $state.go', function() {
      $state.go('routes');
      $rootScope.$apply();
      expect($state.is('routes'));
    });
  });
});
