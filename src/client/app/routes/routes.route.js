(function() {
  'use strict';

  angular
    .module('app.routes')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'routes',
        config: {
          url: '/routes',
          templateUrl: 'app/routes/routes.html',
          controller: 'RoutesController',
          controllerAs: 'vm',
          title: 'Routes',
          settings: {
            nav: 2,
            content: '<i class="fa fa-code-fork" title="Routes"></i>'
          }
        }
      }
    ];
  }
})();
