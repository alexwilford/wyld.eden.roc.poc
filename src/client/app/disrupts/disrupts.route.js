(function() {
  'use strict';

  angular
    .module('app.disrupts')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'disrupts',
        config: {
          url: '/disrupts',
          templateUrl: 'app/disrupts/disrupts.html',
          controller: 'DisruptsController',
          controllerAs: 'vm',
          title: 'Disruptions',
          settings: {
            nav: 5,
            content: '<i class="fa fa-warning" title="Disruptions"></i>'
          }
        }
      }
    ];
  }
})();
