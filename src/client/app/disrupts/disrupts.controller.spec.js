/* jshint -W117, -W030 */
describe('DisruptsController', function() {
  var controller;

  beforeEach(function() {
    bard.appModule('app.disrupts');
    bard.inject('$controller', '$log', '$rootScope');
  });

  beforeEach(function() {
    controller = $controller('DisruptsController');
    $rootScope.$apply();
  });

  bard.verifyNoOutstandingHttpRequests();

  describe('Disrupts controller', function() {
    it('should be created successfully', function() {
      expect(controller).to.be.defined;
    });

    describe('after activate', function() {
      it('should have title of Disrupts', function() {
        expect(controller.title).to.equal('Disrupts');
      });

      it('should have logged "Activated"', function() {
        expect($log.info.logs).to.match(/Activated/);
      });
    });
  });
});
