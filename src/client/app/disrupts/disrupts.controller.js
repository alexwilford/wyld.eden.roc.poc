(function() {
  'use strict';

  angular
    .module('app.disrupts')
    .controller('DisruptsController', DisruptsController);

  DisruptsController.$inject = ['logger', 'dataservice'];
  /* @ngInject */
  function DisruptsController(logger, dataservice) {
    var vm = this;
    vm.title = 'Disruption hub';
    vm.subtitle = 'Disruptions';

    activate();

    function activate() {
      dataservice.setTheme();
      logger.info('Activated Disruptions View');
    }
  }
})();
