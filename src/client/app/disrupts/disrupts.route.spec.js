/* jshint -W117, -W030 */
describe('disrupts routes', function() {
  describe('state', function() {
    var view = 'app/disrupts/disrupts.html';

    beforeEach(function() {
      module('app.disrupts', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state disrupts to url /disrupts ', function() {
      expect($state.href('disrupts', {})).to.equal('/disrupts');
    });

    it('should map /disrupts route to disrupts View template', function() {
      expect($state.get('disrupts').templateUrl).to.equal(view);
    });

    it('of disrupts should work with $state.go', function() {
      $state.go('disrupts');
      $rootScope.$apply();
      expect($state.is('disrupts'));
    });
  });
});
