(function() {
  'use strict';

  angular.module('app.disrupts', [
    'app.core',
    'app.widgets'
  ]);

})();
