(function() {
  'use strict';

  angular
    .module('app.comms')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'comms',
        config: {
          url: '/comms',
          templateUrl: 'app/comms/comms.html',
          controller: 'CommsController',
          controllerAs: 'vm',
          title: 'Comms',
          settings: {
            nav: 6,
            content: '<i class="fa fa-comments" title="Comms"></i>'
          }
        }
      }
    ];
  }
})();
