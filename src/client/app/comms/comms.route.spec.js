/* jshint -W117, -W030 */
describe('comms routes', function() {
  describe('state', function() {
    var view = 'app/comms/comms.html';

    beforeEach(function() {
      module('app.comms', bard.fakeToastr);
      bard.inject('$httpBackend', '$location', '$rootScope', '$state', '$templateCache');
    });

    beforeEach(function() {
      $templateCache.put(view, '');
    });

    it('should map state comms to url /comms ', function() {
      expect($state.href('comms', {})).to.equal('/comms');
    });

    it('should map /comms route to comms View template', function() {
      expect($state.get('comms').templateUrl).to.equal(view);
    });

    it('of comms should work with $state.go', function() {
      $state.go('comms');
      $rootScope.$apply();
      expect($state.is('comms'));
    });
  });
});
