(function() {
  'use strict';

  angular.module('app.comms', [
    'app.core',
    'app.widgets',
    'nemLogging',
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap',
    'ui-leaflet'
  ]);

})();
