(function() {
  'use strict';

  angular
    .module('app.comms')
    .controller('CommsController', CommsController);

  CommsController.$inject = ['$q', '$scope', 'logger', 'dataservice', 'leafletData', 'leafletBoundsHelpers'];
  /* @ngInject */
  function CommsController($q, $scope, logger, dataservice, leafletData, leafletBoundsHelpers) {
    var vm = this;
    vm.title = 'Communications';
    vm.subtitle = 'Check-in/Check-out';
    vm.staff = [];
    vm.stations = [];
    vm.filtered = [];
    vm.staffMember = {};
    vm.sortProperty = 'no';
    vm.sortReverse = false;
    vm.allSelected = true;
    vm.showChat = false;
    vm.btnSelection = '';


    activate();

    function activate() {
      dataservice.setTheme();
      var promises = [
        getStaff(),
        getStations()
      ];
      return $q.all(promises).then(function() {
        $scope.$watch('vm.filtered', function(newVal, oldVal){
          vm.addStaff(newVal);
        }, true);
        logger.info('Activated Comms View');
      });
    }


    function getStaff() {
      return dataservice.getStaff().then(function(data) {
        vm.staff = data;
        vm.filtered = data;
        vm.staffMember = data[0];
        vm.groups = _.uniq(_.pluck(data, 'group')).sort();
        vm.status = _.uniq(_.pluck(data, 'online'));
        return vm.staff;
      });
    }

    function getStations() {
      return dataservice.getStations().then(function(data) {
        vm.stations = data;
        return vm.stations;
      });
    }

    function getNames(array) {
      if (!array) { array = vm.filtered; }
      vm.names = _.uniq(_.pluck(array, 'name')).join('; ');
    }

    vm.sortBy = function(property) {
      if (property === vm.sortProperty) {
        vm.sortReverse=!vm.sortReverse;
      }
      vm.sortProperty = property;
    };

    // locate staff member
    vm.locate = function(s) {
      vm.staffMember = s;

      var message = getStaffPopupText(s);
      var station = getStationDetail(s.station);
      var lat = station.lat;
      var lng = station.lng;

      zoomTo(lat,lng);

      vm.markers[s.no] = {
        lat: lat,
        lng: lng,
        message: message,
        focus: true
      };
    };

    function zoomTo(lat,lng) {
      vm.home = {
        lat: lat,
        lng: lng,
        zoom: vm.defaults.maxZoom
      };
    }

    function zoomToBounds(minlat, minlng, maxlat, maxlng) {
      if (minlat < 90) {
        vm.home = {};
        vm.bounds = leafletBoundsHelpers.createBoundsFromArray([
          [ minlat, minlng ],
          [ maxlat, maxlng ]
        ]);
        //console.log(vm.bounds);
      } else {
        returnHome();
      }
    }

    function returnHome(z) {
      vm.home = {
        lat: 54.5,
        lng: -2.5,
        zoom: z || 5
      };
    }

    function getStaffPopupText(s) {
      var HTML = '<div><img class="img-team" src="' + s.image +
        '" width="120px" alt="' + s.name + '"><br>' +
        '<div><span style="font-size: 20px">' +
        '<i class="' + s.symbol + ' ' + s.status + '"></i> ' + s.name +
        '</span></div>' +
        '<b>Last seen:</b> ' + s.lastseen + '<br>' +
        '<b>Location:</b> ' + s.location + '</div>';
      return HTML;
    }

    function getStationsPopupText(s) {
      var img = s.code;
      if (s.level > 1) { img = 'local'; }
      var HTML = '<div><img src="images/stations/' + img + '.jpg" width="200px">' +
        '<div><span style="font-size: 20px">' + s.name + '</span>' +
        '<br>Lat: ' + s.lat +
        '<br>Lng: ' + s.lng +
        '<br>ID: ' + s.stationid +
        '<br>Station code: ' + s.code +
        '</div></div>';
      return HTML;
    }

    vm.removeMarkers = function() {
      vm.markers = {};
      vm.geojson = {};
    };

    function getStationDetail(stationCode) {
      return _.findWhere(vm.stations, {
          code: stationCode
        });
    }

    // add all stations to map
    vm.addStations = function() {
      var jsonFeatures = [];
      vm.removeMarkers();

      vm.stations.forEach(function(station) {
        var lat = station.lat;
        var lng = station.lng;

        var feature = {
          type: 'Feature',
          id: station.code,
          name: station.name,
          properties: station,
          geometry: {
            type: 'Point',
            coordinates: [lng, lat]
          }
        };
        jsonFeatures.push(feature);
      });

      vm.geojson.data = {
        type: 'FeatureCollection',
        features: jsonFeatures
      };

      var bindPopupContent = function(feature, layer) {
          var HTML = getStationsPopupText(feature.properties);
          layer.bindPopup(HTML);
        };
      vm.geojson.options = {
        onEachFeature: bindPopupContent
      };

      returnHome(6);
    };

    // add all staff to map
    vm.addStaff = function(staffMembers) {
      vm.removeMarkers();

      var jsonFeatures = [];
      var filteredList = [];
      var lat, lng;
      var minlat = 90;
      var maxlat = -90;
      var minlng = 180;
      var maxlng = -180;

      if (!staffMembers) {
        staffMembers = vm.staff;
      }
      staffMembers.forEach(function(s) {
        if (s.selected) {
          var station = getStationDetail(s.station);
          lat = station.lat;
          lng = station.lng;

          if (lat < minlat) { minlat = lat; }
          if (lat > maxlat) { maxlat = lat; }
          if (lng < minlng) { minlng = lng; }
          if (lng > maxlng) { maxlng = lng; }

          var feature = {
            type: 'Feature',
            id: s.no,
            name: s.name,
            properties: s,
            geometry: {
              type: 'Point',
              coordinates: [lng, lat]
            }
          };
          jsonFeatures.push(feature);
          filteredList.push(s);
        }
      });

      vm.geojson.data = {
        type: 'FeatureCollection',
        features: jsonFeatures
      };

      var bindPopupContent = function(feature, layer) {
          var HTML = getStaffPopupText(feature.properties);
          layer.bindPopup(HTML);
        };
      vm.geojson.options = {
        onEachFeature: bindPopupContent
      };

      zoomToBounds(minlat, minlng, maxlat, maxlng);
      getNames(filteredList);
    };


    vm.toggleChat = function(s) {
      vm.staffMember = s;
      vm.showChat = true;
    };

    vm.toggleAllStaff = function() {
      vm.allSelected=!vm.allSelected;
      vm.staff.forEach(function(s) {
        s.selected = vm.allSelected;
      });
    };

    vm.getDate = function(n) {
      var d = new Date();
      if (n) {
        d.setDate(d.getDate() + n);
      }
      return d;
    };


    // Leaflet map
    angular.extend(vm, {
      home: {
        lat: 54.5,
        lng: -2.5,
        zoom: 6
      },
      tiles: {
        url: 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
      },
      defaults: {
        minZoom: 2,
        maxZoom: 17,
        zoomControlPosition: 'topright',
        scrollWheelZoom: true
      },
      bounds: [],
      geojson: {},
      markers: {
        home: {
          lat: 54.5,
          lng: -2.5,
          message: '',
          focus: false
        }
      }
    });

  }
})();
