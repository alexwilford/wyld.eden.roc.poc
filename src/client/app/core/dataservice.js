(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('dataservice', dataservice);

  dataservice.$inject = ['$http', '$q', 'exception', 'logger'];
  /* @ngInject */
  function dataservice($http, $q, exception, logger) {
    var service = {
      getCards: getCards,
      getStaff: getStaff,
      getStations: getStations,
      getServices: getServices,
      getUserConfig: getUserConfig,
      setUserConfig: setUserConfig,
      setTheme: setTheme,
      getMessageCount: getMessageCount
    };

    return service;

    function getMessageCount() { return $q.when(72); }

    function getCards() {
      return $http.get('/api/cards')
        .then(success)
        .catch(fail);
      function success(response) {
        return response.data;
      }
      function fail(e) {
        return exception.catcher('XHR Failed for getCards')(e);
      }
    }

    function getStaff() {
      return $http.get('/api/staff')
        .then(success)
        .catch(fail);
      function success(response) {
        return response.data;
      }
      function fail(e) {
        return exception.catcher('XHR Failed for getStaff')(e);
      }
    }

    function getStations() {
      return $http.get('/api/stations')
        .then(success)
        .catch(fail);
      function success(response) {
        return response.data;
      }
      function fail(e) {
        return exception.catcher('XHR Failed for getStations')(e);
      }
    }

    function getServices() {
      return $http.get('/api/services')
        .then(success)
        .catch(fail);
      function success(response) {
        return response.data;
      }
      function fail(e) {
        return exception.catcher('XHR Failed for getServices')(e);
      }
    }

    function setUserConfig(item, value) {
      localStorage.setItem(item, value);
    }

    function getUserConfig(item) {
      return localStorage.getItem(item);
    }

    function setTheme(theme) {
      if (!theme) {
        theme = getUserConfig('theme');
      }
      setUserConfig('theme', theme);
      if (theme === 'dark') {
        less.modifyVars({
          '@color_theme': 'dark',
          '@color_background': '#2a3b49',
          '@color_highlight': '#54626d',
          '@color_shadow': '#000000',
          '@color_widget': '#1e2d3a',
          '@color_border': '#1e2d3a',
          '@color_table': '#2a3b49',
          '@color_font': '#ffffff',
          '@color_title': '#ffffff',
          '@color_message': '#c1c1c1'
        });
      } else {
        less.modifyVars({
          '@color_theme': 'light',
          '@color_background': '#f1f1f1',
          '@color_highlight': '#eeeeee',
          '@color_shadow': '#c1c1c1',
          '@color_widget': '#ffffff',
          '@color_border': '#dddddd',
          '@color_table': '#f9f9f9',
          '@color_font': '#2a3b49',
          '@color_title': '#2a3b49',
          '@color_message': '#808080'
        });
      }
    }

  }
})();
