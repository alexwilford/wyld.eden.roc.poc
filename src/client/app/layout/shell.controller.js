(function() {
  'use strict';

  angular
    .module('app.layout')
    .controller('ShellController', ShellController);

  ShellController.$inject = ['$rootScope', '$timeout', '$location', 'config', 'logger'];
  /* @ngInject */
  function ShellController($rootScope, $timeout, $location, config, logger) {
    var vm = this;
    vm.busyMessage = 'Please wait ...';
    vm.isBusy = true;
    $rootScope.showSplash = true;
    vm.navline = {
      title: config.appTitle,
      text: config.appFullTitle,
      company: 'Wyld Technologies Ltd',
      link: 'http://www.wyldtechnologies.com',
      logo: 'images/logos/virgin_trains.png'
    };

    activate();

    function activate() {
      logger.success(config.appTitle + ' loaded!', null);
      hideSplash();
    }

    function hideSplash() {
      //Force a 1 second delay so we can see the splash.
      $timeout(function() {
        $rootScope.showSplash = false;
      }, 1000);
    }

    vm.toggleHome = function() {
      // console.log($location.path);
      // if (home) {
      //   $location.path('/comms');
      // } else {
      //   $location.path('/');
      // }
    };

  }
})();
