$(function () {
    "use strict";
    // for better performance - to avoid searching in DOM
    //var content = $('#sigma-content');


    var checkExist = setInterval(function() {
   if ($('#sigma-content').length) {

      var content = document.getElementById('sigma-content');

      // if user is running mozilla then use it's built-in WebSocket
      window.WebSocket = window.WebSocket || window.MozWebSocket;

      // if browser doesn't support WebSocket, just show some notification and exit
      if (!window.WebSocket) {
          content.html($('<p>', { text: 'Sorry, but your browser doesn\'t support WebSockets.'} ));
          input.hide();
          $('span').hide();
          return;
      }

      // open connection
      var connection = new WebSocket('ws://127.0.0.1:1337');
      var s2 = new sigma(document.getElementById('sigma-container'));
      s2.settings({
        edgeColor: 'default',
        defaultEdgeColor: '#800080',
        minEdgeSize: 2,
        maxEdgeSize: 4,
        minNodeSize: 8,
        maxNodeSize: 12,
        minArrowSize: 10
      });


      connection.onopen = function () {
          // first we want users to enter their names
          //input.removeAttr('disabled');
          //status.text('Choose name:');
      };

      connection.onerror = function (error) {
          content.innerHTML='Sorry, but there\'s some problem with your connection or the server is down.';
      };

      /*
      an: Add node
      cn: Change node
      dn: Delete node
      ae: Add edge
      ce: Change edge
      de: Delete edge
      lo: Load Graph
      sv: Save Graph
      ex: Exit
      */

      // most important part - incoming messages
      connection.onmessage = function (message) {
          // try to parse JSON message. Because we know that the server always returns
          // JSON this should work without any problem but we should make sure that
          // the massage is not chunked or otherwise damaged.
          try {
              //console.log('>'+ json.an);
              var json = JSON.parse(message.data);

              if (json.an) //Add Node
              {
                //console.log('an: ',json.an);
                addNode(json.an);
              }
              else if (json.dn) //Delete Node
              {
                  //console.log('dn: ',json.dn);
                  deleteNode(json.dn);
              }
              else if (json.ae) //Add Edge
              {
                  //console.log('ae: ',json.ae);
                  addEdge(json.ae);//json.ae = {"target":"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"}
              }
              else if (json.de) //Delete Edge
              {
                  //console.log('de: ',json.de);
                  deleteEdge(json.de);//json.ae = {"target":"XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"}
              }
              else if (json.lo) //Load
              {
                  //console.log('lo: ',json.lo);
              }
              else {
                console.log('Unknown Verb >> ',json);
              }
              //display
              addMessage(message.data, 'Black', new Date());//json.data.time));


          } catch (e) {

              console.log('Exception: ' + e);
              addMessage(e,'Black', new Date());//json.data.time));
              return;
          }
      };



      /**
       * This method is optional. If the server wasn't able to respond to the
       * in 3 seconds then show some error message to notify the user that
       * something is wrong.
       */
      setInterval(function() {
          if (connection.readyState !== 1) {
              //status.text('Error');
              //input.attr('disabled', 'disabled').val('Unable to communicate '
            //                                       + 'with the WebSocket server.');
          }
      }, 3000);

      /**
       * Add message to the 'content' element id
       */
      function addMessage(message, color, dt) {
        var hh = ('0' + dt.getHours()).slice(-2);
        var mm = ('0' + dt.getMinutes()).slice(-2);
        content.innerHTML += '<p><span style="color:' + color + '">' +
              '@' + hh + ':' +  mm + ' -- ' + message + '</span></p>';
      }

      /*
      an: Add node
      cn: Change node
      dn: Delete node
      ae: Add edge
      ce: Change edge
      de: Delete edge
      lo: Load Graph
      sv: Save Graph
      */

      /**
       * addNode
      * {"an":{"xxxxxxxx-xxx-xxx-xxx-xxxxxxxxxxxx":{"label":"Gene","size":51}}}');
       */
      function addNode(json) {

        //s2.stopForceAtlas2();
        //console.log('add node: ',json);
        //var myGraph = new sigma.classes.graph();
        //var s = new sigma.classes.graph('container');

        var LABEL = Object.keys(json)[0];
        var OBJECT = Object.values(json)[0];
        var ID = LABEL;//'n' + (s2.graph.nodes().length);

        s2.graph.addNode({
          // Main attributes:
          id: ID,
          label: LABEL,
          // Display attributes:
          x: s2.graph.nodes().length/2+Math.random() ,
          y: s2.graph.nodes().length/2+Math.random() ,
          size: OBJECT.size,
          color: '#800080'
        });


        s2.startForceAtlas2();
        s2.refresh();

        //console.log('id: '+ ID + ' size: ' + OBJECT.size + ' label: ' + OBJECT.label + '/' + LABEL + ' nodeCount: ' + s2.graph.nodes().length);
      }

      /**
       * changeNode
       */
      function changeNode(graph,json) {

      }
      /**
       * deleteNode
       */
      function deleteNode(json) {
        var ID = Object.keys(json)[0];
        //console.log("Delete ",ID);
        s2.graph.dropNode(ID);

        s2.refresh();
        s2.startForceAtlas2();
      }

      /**
       * addEdge       var ID = Object.keys(json)[0];

       */
      function addEdge(json) {

          var ID = Object.keys(json)[0];
          var OBJECT = Object.values(json)[0];
          s2.graph.addEdge({
            // Main attributes:
            id: ID + "-" + OBJECT.target,
            source: ID,
            target: OBJECT.target,
            type:'curved',
            size: 1,
            color: 'purple'
          });



            //ID + "-" + OBJECT.target, ID, OBJECT.target);
          s2.refresh();
          s2.startForceAtlas2();

      }

      /**
       * changeEdge
       */
      function changeEdge(graph,json) {

      }
      /**
       * deleteEdge
       */
      function deleteEdge(json) {

        var ID = Object.keys(json)[0];
        var OBJECT = Object.values(json)[0];
        console.log("Delete EDGE:::: %j",ID + "-" + OBJECT.target);
        s2.graph.dropNode(ID + "-" + OBJECT.target);



          //ID + "-" + OBJECT.target, ID, OBJECT.target);
        s2.refresh();
        s2.startForceAtlas2();
      }

      /**
       * loadGraph
       */
      function loadGraph(json) {
        console.log ("load graph");
        sigInst.stopForceAtlas2();
        $.getJSON(json, function (data) {
            var edges = data.edges;
            var nodes = data.nodes;
            $.each(nodes, function (index, value) {
                addNode(value.label, value.x, value.y, value.id, value.size, value.color);
            });
            $.each(edges, function (index, value) {
                addEdge(value.source, value.target, value.id, value.weight);
            });
        });
        sigInst.draw(2,2,2);
      }


      /**
       * saveGraph
       */
      function saveGraph(graph,json) {

      }

      clearInterval(checkExist);
   }
}, 100); // check every 100ms







});
